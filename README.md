# Ordenes de compra en sucursales

Proyecto demo para crear ordenes de compra en sucursales

## Comenzando 🚀

El proyecto consta de sucursales, productos y órdenes de compra


### Utilizando 🔧

La finalidad de este proyecto de evaluacion es poder crear una orden de compra enviando toda la información necesaria al servidor
por ejemplo un json como el siguiente:
```
{
    "sucursal" :{
        "nombre" : "CDMX"
    },
    "productos" : [{
            "codigo": "18156",
            "descripcion" : "Esmeriladora angular",
            "precio" : 625
    },
    {
            "codigo": "17193",
            "descripcion" : "Pala redonda",
            "precio" : 100.50
    }
    ],
    "fecha" : "2022-02-21T04:22:00",
    "total" : 725.50
}
```

Sin embargo, el postman probado hasta el momento es el de a continuación ya que no terminé todos los puntos mencionados en las instrucciones 
hasta el momento de finalizar la prueba: 
```
{
    "sucursal" :{
        "nombre" : "CDMX"
    },    
    "fecha" : "2022-02-21T04:22:00",
    "total" : 725.50
}
```