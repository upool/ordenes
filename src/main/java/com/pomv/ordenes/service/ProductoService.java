package com.pomv.ordenes.service;

import java.util.List;

import com.pomv.ordenes.entity.Producto;

public interface ProductoService {
	
	
	List<Producto> listarProductos();
	
	Producto crear(Producto producto);
	
	Producto actualizar (Producto producto);
	

}
