package com.pomv.ordenes.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pomv.ordenes.entity.Sucursal;
import com.pomv.ordenes.repository.SucursalRepository;

@Service
public class SucursalServiceImp implements SucursalService {

	
	@Autowired
	SucursalRepository sucursalRepo;
	
	@Override
	public List<Sucursal> listarSucursales() {
		return this.sucursalRepo.findAll();
	}

	@Override
	public Sucursal crear(Sucursal sucursal) {
		Sucursal suc = this.sucursalRepo.save(sucursal);
		return suc;
	}

	
}
