package com.pomv.ordenes.service;

import java.util.List;

import com.pomv.ordenes.entity.Sucursal;

public interface SucursalService {
	
	
	List<Sucursal> listarSucursales();
	
	Sucursal crear(Sucursal sucursal);

}
