package com.pomv.ordenes.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pomv.ordenes.dto.NuevaOrdenDTO;
import com.pomv.ordenes.entity.Orden;
import com.pomv.ordenes.entity.Sucursal;
import com.pomv.ordenes.repository.OrdenRepository;

@Service
public class OrdenServiceImpl implements OrdenService {

	
	@Autowired
	OrdenRepository ordenRepo;
	
	@Autowired
	SucursalService sucursalService;
	
	@Override
	public List<Orden> listarOrdenes() {
		List<Orden> listaOrden = this.ordenRepo.findAll();
		return listaOrden;
	}

	@Override
	public Orden crear(Orden orden) {		
		
		Sucursal suc = this.sucursalService.crear(orden.getSucursal());
		// orden.getSucursal().setId(suc.getId());;
		orden.setSucursal(suc);
		Orden newOrden = this.ordenRepo.save(orden);
		return newOrden;
	}

//	@Override
//	public Orden nuevaOrden(NuevaOrdenDTO ordenParam) {
//		
//		Orden orden = ordenParam.get
//		
//		Sucursal suc = this.sucursalService.crear(orden.getSucursal());
//		orden.setSucursal(suc);
//		
//		Orden newOrden = this.ordenRepo.save(orden);
//		return newOrden;
//		
//	}

}
