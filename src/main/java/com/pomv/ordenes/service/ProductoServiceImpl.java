package com.pomv.ordenes.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pomv.ordenes.entity.Producto;
import com.pomv.ordenes.repository.ProductoRespository;


@Service
public class ProductoServiceImpl implements ProductoService {

	
	@Autowired
	ProductoRespository productoRepo;
	
	@Override
	public List<Producto> listarProductos() {
		return this.productoRepo.findAll();
	}

	@Override
	public Producto crear(Producto producto) {
		Producto prod = this.productoRepo.save(producto);
		return prod;
	}

	@Override
	public Producto actualizar(Producto producto) {
		Producto prod = this.productoRepo.save(producto);
		return prod;
	}
	

}
