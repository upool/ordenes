package com.pomv.ordenes.service;

import java.util.List;

import com.pomv.ordenes.entity.Orden;

public interface OrdenService {
	
	
	
	List<Orden> listarOrdenes();
	
	Orden crear(Orden orden);
	
//	Orden nuevaOrden(NuevaOrdenDTO orden);

}
