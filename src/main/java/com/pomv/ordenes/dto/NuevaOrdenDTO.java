package com.pomv.ordenes.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pomv.ordenes.entity.Producto;
import com.pomv.ordenes.entity.Sucursal;

import lombok.Data;

public class NuevaOrdenDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8693869715822183246L;
	
	 private Sucursal sucursal;
	 private List<Producto> productos;
	 private Date fecha;
	 private double total;
	public Sucursal getSucursal() {
		return sucursal;
	}
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}
	public List<Producto> getProductos() {
		return productos;
	}
	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
	 
	 

}
