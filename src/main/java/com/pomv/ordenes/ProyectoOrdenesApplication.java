package com.pomv.ordenes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoOrdenesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoOrdenesApplication.class, args);
	}

}
