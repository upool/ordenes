package com.pomv.ordenes.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pomv.ordenes.entity.Orden;
import com.pomv.ordenes.entity.Producto;
import com.pomv.ordenes.entity.Sucursal;
import com.pomv.ordenes.service.OrdenService;
import com.pomv.ordenes.service.ProductoService;
import com.pomv.ordenes.service.SucursalService;

@RestController
@RequestMapping("/orden")
public class OrdenesController {

	
	@Autowired
	ProductoService productoService;
	
	@Autowired
	SucursalService sucursalService;

	@Autowired
	OrdenService ordenService;
	
	
	
	@GetMapping("/")
	public ResponseEntity<?> listarOrdenes(){
		
		try {
			List<Orden> ordenList = this.ordenService.listarOrdenes();
			return ResponseEntity.ok(ordenList);		
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Error al consultar órdenes");
		}
	}
	
	
	@PostMapping("/")
	public ResponseEntity<?> crearOrden(@RequestBody Orden orden){
		
		try {
			Orden newOrden = this.ordenService.crear(orden);
			return ResponseEntity.ok(newOrden);
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error al crear la orden");
		}
	}
	
	
//	@PostMapping("/")
//	public ResponseEntity<?> crearOrden(@RequestBody NuevaOrdenDTO orden){
//		
//		try {
//			Orden newOrden = this.ordenService.nuevaOrden(orden);
//			return ResponseEntity.ok(newOrden);
//		}catch(Exception ex) {
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error al crear la orden");
//		}
//	}
	
	
	
	
	
	@GetMapping("/sucursal")
	public ResponseEntity<?> listarSucursales(){
		
		try {
			List<Sucursal> sucursalList = this.sucursalService.listarSucursales();
			return ResponseEntity.ok(sucursalList);		
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Error al consultar sucursales");
		}
	}
	
	@PostMapping("/sucursal")
	public ResponseEntity<?> crearSucursal(@RequestBody Sucursal sucursal){
		
		try {
			Sucursal sucur = this.sucursalService.crear(sucursal);
			return ResponseEntity.ok(sucur);		
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error al crear la sucursal");
		}
	}

	
	
		
	
	@GetMapping("/producto")
	public ResponseEntity<?> listarProductos(){
		
		try {
			List<Producto> productosList = this.productoService.listarProductos();
			return ResponseEntity.ok(productosList);		
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Error al consultar productos");
		}
	}
	
	@PostMapping("/producto")
	public ResponseEntity<?> crearProducto(@RequestBody Producto producto){
		
		try {
			Producto prod = this.productoService.crear(producto);
			return ResponseEntity.ok(prod);		
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error al crear el producto");
		}
	}
	
	
	
	
	
	
}
